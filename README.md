## Python 3 na Web com Django Basico/intermediario

Used:
Python3.4
Django==1.9

## Blog: https://simplemooc-masedos.c9.io/

Tutorial by https://www.udemy.com/python-3-na-web-com-django-basico-intermediario/learn/#

1) Initialized empty Git repository in ~/simplemooc/.git/

    $ git init  
    $ git config user.name "Fernandes Macedo"
    $ git config user.email masedos@egmail.com
    $ git status
    $ git add -A .
    $ git commit -m "first commit"

2) Create a new repository on github trydjango

    $ git remote add origin https://masedos@bitbucket.org/masedos/simplemooc.git
    $ git push -u origin master


## Starting from the Terminal

In case you want to run your Django application from the terminal just run:

3) Run migrate command to sync models to database and create Django's default superuser and auth system

    $ python manage.py makemigrations
    $ python manage.py migrate

4) Run Django

    $ python manage.py runserver $IP:$PORT
    
    
5) Change the version of python

    sudo ln -nsf /usr/bin/python3.4  /usr/bin/python 
    